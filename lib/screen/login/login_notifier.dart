import 'package:chat_app/screen/login/login_state.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter/material.dart';

import '../../app_state/main_app_state.dart';

class LoginNotifier extends StateNotifier<LoginState> {
  LoginNotifier() : super(const LoginState());

  TextEditingController userNameController = TextEditingController();
  TextEditingController passWordController = TextEditingController();

  Future<void> login(MainAppState mainAppState) async {
    state = state.copyWith(isLoading: true);

  }

  void showPassWord() {
    state = state.copyWith(showPassWord: !state.showPassWord);
  }

  bool isValidUserName(String? username) {
    if (username?.isEmpty ?? true) return false;
    return true;
  }

  bool isValidPassWord(String? password) {
    if ((password?.isEmpty ?? true)) return false;
    return true;
  }

  void validInfoLogin() {
    if (!isValidUserName(userNameController.text.trim()) ||
        !isValidPassWord(passWordController.text.trim())) {
      state = state.copyWith(isValidLogin: false);
    } else {
      state = state.copyWith(isValidLogin: true);
    }
  }

  void resetLoginState() {
    userNameController.text = '';
    passWordController.text = '';
    state = state.copyWith(
      isLoading: false,
      isValidLogin: false,
      showPassWord: false,
    );
  }

  @override
  void dispose() {
    super.dispose();
    userNameController.dispose();
    passWordController.dispose();
  }
}