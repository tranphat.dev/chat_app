import 'package:chat_app/screen/login/login_notifier.dart';
import 'package:chat_app/screen/login/login_state.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final loginNotifierProvider = StateNotifierProvider<LoginNotifier, LoginState>(
      (ref) => LoginNotifier(),
);