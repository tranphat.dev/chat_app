import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../app_state/main_app_state.dart';
import '../../components/labeled_input.dart';
import '../../components/password_input.dart';
import 'login_provider.dart';

class LoginView extends ConsumerStatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  LoginViewState createState() => LoginViewState();
}

class LoginViewState extends ConsumerState<LoginView> {
  @override
  void initState() {
    super.initState();
    ref.read(mainAppStateProvider);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _buildLoginForm(context),
      ),
    );
  }

  Widget _buildLoginForm(BuildContext context) {
    final _loginNotifier = ref.read(loginNotifierProvider.notifier);
    final _loginState = ref.read(loginNotifierProvider);
    final _mainAppState = ref.read(mainAppStateProvider);
    return Container(
      padding: const EdgeInsets.only(left: 16, right: 16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'Login',
            style: Theme.of(context).textTheme.subtitle2?.copyWith(
              fontSize: 16,
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 40),
          LabeledInput(
            hintText: 'Username',
            controller: _loginNotifier.userNameController,
            enabled: true,
            disableTextError: true,
            onChanged: (_) {
              _loginNotifier.validInfoLogin();
            },
          ),
          const SizedBox(height: 20),
          LabeledPasswordInput(
            controller: _loginNotifier.passWordController,
            hintText: 'Password',
            hidden: !_loginState.showPassWord,
            hiddenToggle: () => _loginNotifier.showPassWord(),
            disableTextError: true,
            onChanged: (_) {
              _loginNotifier.validInfoLogin();
            },
          ),
          const SizedBox(height: 20),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Theme.of(context).primaryColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4.0),
              ),
              side: BorderSide(
                  color: Theme.of(context).primaryColor),
            ),
            onPressed: () {
              _mainAppState.goToTop();
            },
            child: const Text(
              'Login',
              style: TextStyle(color: Colors.white),
            ),
          ),
          const SizedBox(height: 16),
        ],
      ),
    );
  }
}
