import 'package:flutter/material.dart';

class NotFoundPage extends StatelessWidget {
  final VoidCallback onBack;

  const NotFoundPage({Key? key, required this.onBack}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Error'),
      ),
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              const Text('Page does note exist'),
              ElevatedButton(
                  child: const Text('Back'), onPressed: onBack)
            ],
          ),
        ),
      ),
    );
  }
}
