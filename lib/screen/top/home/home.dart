import 'package:chat_app/router/route_path.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../../app_state/top_app_state.dart';


class HomeView extends ConsumerStatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends ConsumerState<HomeView> {
  @override
  Widget build(BuildContext context) {
    final _topAppState = ref.watch(topAppStateProvider);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: Center(
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Theme.of(context).primaryColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4.0),
            ),
            side: BorderSide(
                color: Theme.of(context).primaryColor),
          ),
          onPressed: () {
            ref.read(topAppStateProvider).pushPathToMyPageRoute(ChatSinglePath(id: 1));
          },
          child: const Text(
            'Go to Chat View',
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }
}
