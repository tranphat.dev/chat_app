import 'package:flutter/material.dart';

class ChatSingleView extends StatefulWidget {
  final int id;

  const ChatSingleView({Key? key, required this.id}) : super(key: key);

  @override
  State<ChatSingleView> createState() => _ChatSingleViewState();
}

class _ChatSingleViewState extends State<ChatSingleView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Chat Single'),
      ),
      body: Center(
        child: Text(
          'ID Chat: ${widget.id}',
        ),
      ),
    );
  }
}
