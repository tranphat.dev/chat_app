import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import '../../app_state/top_app_state.dart';
import '../../config/app.dart';
import '../../config/messages.dart';
import '../../router/top_router_delegate.dart';
import '../../utils/helpers.dart';

class TopView extends ConsumerStatefulWidget {
  const TopView({Key? key}) : super(key: key);

  @override
  TopViewState createState() => TopViewState();
}

class TopViewState extends ConsumerState<TopView> {
  late final TopAppState _appState;
  late final TopRouterDelegate _routerDelegate;
  ChildBackButtonDispatcher? _backButtonDispatcher;

  @override
  void initState() {
    super.initState();
    _appState = ref.read(topAppStateProvider);
    _routerDelegate = TopRouterDelegate(_appState);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // Defer back button dispatching to the child router.
    _backButtonDispatcher = Router.of(context)
        .backButtonDispatcher!
        .createChildBackButtonDispatcher();
  }

  @override
  Widget build(BuildContext context) {
    _backButtonDispatcher!.takePriority();
    return Scaffold(
      body: Router(
        routerDelegate: _routerDelegate,
        backButtonDispatcher: _backButtonDispatcher,
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: renderBarItem(),
        currentIndex: _appState.currentTabIndex,
        onTap: (value) {
          setState(() {
            _appState.currentTabIndex = value;
          });
        },
      ),
    );
  }

  List<BottomNavigationBarItem> renderBarItem() {
    List<BottomNavigationBarItem> menus = [];
    List<AppMenu> appMenus = AppConfig.appMenus;
    for (final menu in appMenus) {
      if (menu.isEnable) {
        menus.add(
          BottomNavigationBarItem(
            activeIcon: renderActiveIcon(menu.title),
            icon: renderIcon(menu.title),
            label: menu.title,
          ),
        );
      }
    }
    return menus;
  }

  Widget renderActiveIcon(String title) {
    Widget? widget;
    if (Helpers.isDarkModeEnable(context)) {
      switch (title) {
        case Messages.homeTitle:
          widget = const Icon(Icons.home, color: Colors.white);
          break;
        case Messages.groupTitle:
          widget = const Icon(Icons.group, color: Colors.white);
          break;
        case Messages.profileTitle:
          widget =
              const Icon(Icons.account_circle_rounded, color: Colors.white);
          break;
        default:
          widget = Container();
      }
    } else {
      switch (title) {
        case Messages.homeTitle:
          widget = const Icon(Icons.home, color: Colors.black);
          break;
        case Messages.groupTitle:
          widget = const Icon(Icons.group, color: Colors.black);
          break;
        case Messages.profileTitle:
          widget =
              const Icon(Icons.account_circle_rounded, color: Colors.black);
          break;
        default:
          widget = Container();
      }
    }
    return widget;
  }

  Widget renderIcon(String title) {
    Widget? widget;
    if (Helpers.isDarkModeEnable(context)) {
      switch (title) {
        case Messages.homeTitle:
          widget = const Icon(Icons.home, color: Colors.grey);
          break;
        case Messages.groupTitle:
          widget = const Icon(Icons.group, color: Colors.grey);
          break;
        case Messages.profileTitle:
          widget = const Icon(Icons.account_circle_rounded, color: Colors.grey);
          break;
        default:
          widget = Container();
      }
    } else {
      switch (title) {
        case Messages.homeTitle:
          widget = const Icon(Icons.home, color: Colors.grey);
          break;
        case Messages.groupTitle:
          widget = const Icon(Icons.group, color: Colors.grey);
          break;
        case Messages.profileTitle:
          widget = const Icon(Icons.account_circle_rounded, color: Colors.grey);
          break;
        default:
          widget = Container();
      }
    }
    return widget;
  }
}
