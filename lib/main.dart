import 'package:chat_app/router/main_router_delegate.dart';
// import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'app_state/main_app_state.dart';
import 'app_state/top_app_state.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp(); //configure firebase
  runApp(const ProviderScope(child: MaterialApp(home: ChatApp())));
}

class ChatApp extends ConsumerStatefulWidget {
  const ChatApp({Key? key}) : super(key: key);

  @override
  ChatAppState createState() => ChatAppState();
}

class ChatAppState extends ConsumerState<ChatApp> {
  late final MainRouterDelegate _routerDelegate;

  late final MainRouteInformationParser _routeInformationParser;

  @override
  void initState() {
    super.initState();
    _routerDelegate = MainRouterDelegate(
      mainAppState: ref.read(mainAppStateProvider),
      topAppState: ref.read(topAppStateProvider),
    );
    _routeInformationParser = MainRouteInformationParser();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Chat App',
      themeMode: ThemeMode.system,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      routerDelegate: _routerDelegate,
      routeInformationParser: _routeInformationParser,
    );
  }
}
