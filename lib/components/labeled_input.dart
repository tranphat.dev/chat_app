import 'package:flutter/material.dart';

import '../themes/styles.dart';

class LabeledInput extends StatefulWidget {
  final TextEditingController controller;
  final TextInputType keyboardType;
  final String label;
  final String hintText;
  final bool enabled;
  final String? error;
  final ValueChanged<String>? onChanged;
  final bool disableTextError;

  const LabeledInput({
    Key? key,
    this.label = '',
    this.hintText = '',
    this.error,
    required this.controller,
    this.keyboardType = TextInputType.text,
    this.onChanged,
    this.enabled = true,
    this.disableTextError = false,
  }) : super(key: key);

  @override
  _LabeledInputState createState() => _LabeledInputState();
}

class _LabeledInputState extends State<LabeledInput> {
  final FocusNode _focus = FocusNode();
  Color _backgroundColor = Colors.transparent;

  @override
  void initState() {
    _focus.addListener(_onFocusChange);
    super.initState();
  }

  @override
  void dispose() {
    _focus.dispose();
    super.dispose();
  }

  void _onFocusChange() {
    setState(() {
      if (_focus.hasFocus) {
        _backgroundColor = Styles.pageBackground.withOpacity(0.17);
      } else {
        _backgroundColor = Colors.transparent;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    bool isValid = (widget.error == null || widget.error!.isEmpty);
    bool hasLabel = (widget.label.isNotEmpty);
    if (!isValid) {
      _backgroundColor = Styles.pageBackground.withOpacity(0.17);
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        hasLabel
            ? Text(widget.label, style: Styles.formTitleTextStyle(context))
            : const SizedBox.shrink(),
        hasLabel ? const SizedBox(height: 4) : const SizedBox.shrink(),
        TextFormField(
          controller: widget.controller,
          focusNode: _focus,
          keyboardType: widget.keyboardType,
          enabled: widget.enabled,
          onChanged: widget.onChanged,
          decoration: InputDecoration(
            fillColor: _backgroundColor,
            errorText: isValid ? null : widget.error,
            hintText: widget.hintText,
          ),
        ),
        widget.disableTextError
            ? const SizedBox(width: 0, height: 0)
            : Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const SizedBox(height: 12),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        isValid ? '' : widget.error!,
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2!
                            .copyWith(color: Styles.formErrorColor),
                        textAlign: TextAlign.right,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                  const SizedBox(height: 0),
                ],
              ),
      ],
    );
  }
}
