import 'package:flutter/material.dart';

import '../themes/styles.dart';

class LabeledPasswordInput extends StatefulWidget {
  final TextEditingController controller;
  final TextInputType keyboardType;
  final bool hidden;
  final VoidCallback hiddenToggle;
  final String label;
  final String hintText;
  final String subLabel;
  final String? error;
  final ValueChanged<String>? onChanged;
  final bool disableTextError;

  const LabeledPasswordInput({
    Key? key,
    this.label = '',
    this.hintText = '',
    this.error,
    this.subLabel = '',
    required this.controller,
    required this.hidden,
    required this.hiddenToggle,
    this.onChanged,
    this.keyboardType = TextInputType.text,
    this.disableTextError = false,
  }) : super(key: key);

  @override
  _LabeledPasswordInputState createState() => _LabeledPasswordInputState();
}

class _LabeledPasswordInputState extends State<LabeledPasswordInput> {
  final FocusNode _focus = FocusNode();
  Color _backgroundColor = Colors.transparent;

  @override
  void initState() {
    _focus.addListener(_onFocusChange);
    super.initState();
  }

  @override
  void dispose() {
    _focus.dispose();
    super.dispose();
  }

  void _onFocusChange() {
    setState(() {
      if (_focus.hasFocus) {
        _backgroundColor = Styles.pageBackground.withOpacity(0.17);
      } else {
        _backgroundColor = Colors.transparent;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    bool isValid = (widget.error == null || widget.error!.isEmpty);
    bool hasLabel = (widget.label.isNotEmpty);
    if (!isValid) {
      _backgroundColor = Styles.pageBackground.withOpacity(0.17);
    }
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      hasLabel
          ? Text(widget.label, style: Styles.formTitleTextStyle(context))
          : const SizedBox.shrink(),
      hasLabel ? const SizedBox(height: 4) : const SizedBox.shrink(),
      TextField(
          controller: widget.controller,
          focusNode: _focus,
          keyboardType: widget.keyboardType,
          obscureText: widget.hidden,
          onChanged: widget.onChanged,
          decoration: InputDecoration(
            fillColor: _backgroundColor,
            hintText: widget.hintText,
            errorText: isValid ? null : widget.error,
            suffixIcon: GestureDetector(
              child: Icon(
                widget.hidden
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
                color: Theme.of(context).backgroundColor,
                size: 20,
              ),
              onTap: widget.hiddenToggle,
            ),
          )),
      widget.disableTextError
          ? const SizedBox(width: 0, height: 0)
          : Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(height: 12),
                Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Expanded(
                          child: Text(
                        isValid ? widget.subLabel : widget.error!,
                        style: Theme.of(context).textTheme.bodyText2!.copyWith(
                            fontSize: 12,
                            color: isValid
                                ? Styles.formLabelColor
                                : Styles.formErrorColor),
                        textAlign: TextAlign.right,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      )),
                    ]),
                const SizedBox(height: 0),
              ],
            )
    ]);
  }
}
