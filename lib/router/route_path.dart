abstract class BaseRoutePath {}

class LoginPath extends BaseRoutePath {}

class HomePath extends BaseRoutePath {}

class GroupPath extends BaseRoutePath {}

class ProfilePath extends BaseRoutePath {}

class ChatSinglePath extends BaseRoutePath {
  final int id;
  ChatSinglePath({required this.id});
}
