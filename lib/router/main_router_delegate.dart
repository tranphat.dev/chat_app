import 'package:chat_app/screen/login/login_view.dart';
import 'package:chat_app/screen/top/top.dart';
import 'package:flutter/material.dart';
import 'package:chat_app/router/route_path.dart';

import '../app_state/main_app_state.dart';
import '../app_state/top_app_state.dart';
import '../utils/constants.dart';

class MainRouterDelegate extends RouterDelegate<BaseRoutePath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<BaseRoutePath> {
  @override
  final GlobalKey<NavigatorState> navigatorKey;

  /// All routes state.
  final MainAppState mainAppState;
  final TopAppState topAppState;

  final bool _loggedIn = false;

  MainRouterDelegate({required this.mainAppState, required this.topAppState})
      : navigatorKey = GlobalKey<NavigatorState>() {
    mainAppState.addListener(notifyListeners);
    topAppState.addListener(notifyListeners);

    // Init current page.
    if (_loggedIn) {
      mainAppState.currentPage = Pages.top;
    } else {
      mainAppState.currentPage = Pages.login;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      pages: [
        if (mainAppState.currentPage == Pages.top)
          const MaterialPage(
            key: ValueKey(Constants.topPageKey),
            child: TopView(),
          )
        else ...[
          const MaterialPage(
            key: ValueKey(Constants.loginPageKey),
            child: LoginView(),
          ),
        ]
      ],
      onPopPage: (route, result) {
        if (mainAppState.currentPage == Pages.top) {
          topAppState.handleDispose();

          route.didPop(true);
        } else {
          route.didPop(false);
        }

        notifyListeners();
        return true;
      },
    );
  }

  @override
  Future<void> setNewRoutePath(BaseRoutePath configuration) async {
    if (configuration is LoginPath) {
      mainAppState.currentPage = Pages.login;
    } else if (_loggedIn) {
      mainAppState.currentPage = Pages.top;
    }
  }

  @override
  BaseRoutePath get currentConfiguration {
    switch (mainAppState.currentPage) {
      case Pages.login:
        return LoginPath();
      case Pages.top:
        switch (topAppState.currentTab) {
          case Tabs.home:
            return HomePath();
          case Tabs.group:
            return GroupPath();
          case Tabs.profile:
            return ProfilePath();
          default:
            return HomePath();
        }
      default:
        return _loggedIn ? HomePath() : LoginPath();
    }
  }

  @override
  void dispose() {
    super.dispose();
    mainAppState.removeListener(notifyListeners);
    topAppState.removeListener(notifyListeners);
  }
}

class MainRouteInformationParser extends RouteInformationParser<BaseRoutePath> {
  final bool _loggedIn = false;

  @override
  Future<BaseRoutePath> parseRouteInformation(
      RouteInformation routeInformation) async {
    final uri = Uri.parse(routeInformation.location!);
    if (uri.pathSegments.isEmpty) {
      if (_loggedIn) {
        return HomePath();
      } else {
        return LoginPath();
      }
    } else {
      switch (uri.pathSegments[0]) {
        case Constants.loginRoute:
          return LoginPath();
        case Constants.homeRoute:
          return HomePath();
        case Constants.groupRoute:
          return GroupPath();
        case Constants.profileRoute:
          return ProfilePath();
        default:
          if (_loggedIn) {
            return HomePath();
          } else {
            return LoginPath();
          }
      }
    }
  }

  @override
  RouteInformation? restoreRouteInformation(BaseRoutePath configuration) {
    if (configuration is LoginPath) {
      return const RouteInformation(location: '/${Constants.loginRoute}');
    }
    if (configuration is HomePath) {
      return const RouteInformation(location: '/${Constants.homeRoute}');
    }
    if (configuration is GroupPath) {
      return const RouteInformation(location: '/${Constants.groupRoute}');
    }
    if (configuration is ProfilePath) {
      return const RouteInformation(location: '/${Constants.profileRoute}');
    }
    return null;
  }
}
