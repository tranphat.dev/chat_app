import 'package:chat_app/router/route_path.dart';
import 'package:chat_app/screen/top/chat_single/chat_single.dart';
import 'package:chat_app/screen/top/group/group.dart';
import 'package:chat_app/screen/top/home/home.dart';
import 'package:chat_app/screen/top/profile/profile.dart';
import 'package:flutter/material.dart';
import '../app_state/top_app_state.dart';
import '../components/fade_animation_page.dart';
import '../screen/not_found/not_found.dart';
import '../utils/constants.dart';

class TopRouterDelegate extends RouterDelegate<BaseRoutePath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<BaseRoutePath> {
  @override
  final GlobalKey<NavigatorState> navigatorKey;

  final TopAppState _appState;

  TopRouterDelegate(this._appState)
      : navigatorKey = GlobalKey<NavigatorState>() {
    _appState.addListener(() {
      notifyListeners();
    });
  }

  @override
  Widget build(BuildContext context) {
    final myPageRoute = _appState.myPageRoute;
    return Navigator(
      key: navigatorKey,
      pages: [
        // Home tab
        if (_appState.currentTab == Tabs.home) ...[
          const FadeAnimationPage(
            key: ValueKey(Constants.homePageKey),
            child: HomeView(),
          ),
        ]
        // Group tab
        else if (_appState.currentTab == Tabs.group) ...[
          const FadeAnimationPage(
            key: ValueKey(Constants.groupPageKey),
            child: GroupView(),
          ),
        ]
        // Profile tab
        else if (_appState.currentTab == Tabs.profile) ...[
          const FadeAnimationPage(
            key: ValueKey(Constants.profilePageKey),
            child: ProfileView(),
          ),
        ],
        if (myPageRoute.isNotEmpty)
          ...myPageRoute.map(
            (e) {
              if (e is ChatSinglePath) {
                return MaterialPage(
                  key: const ValueKey(Constants.chatSinglePageKey),
                  child: ChatSingleView(id: e.id),
                );
              }
              return MaterialPage(
                key: const ValueKey(Constants.notFoundKey),
                child: NotFoundPage(
                  onBack: () => Navigator.pop(context),
                ),
              );
            },
          )
      ],
      onPopPage: (route, result) {
        _appState.handlePopPage();
        notifyListeners();
        return route.didPop(result);
      },
    );
  }

  @override
  Future<void> setNewRoutePath(BaseRoutePath configuration) async {
    assert(false);
  }
}
