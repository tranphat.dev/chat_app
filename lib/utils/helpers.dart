import 'package:flutter/material.dart';

class Helpers {
  static bool isDarkModeEnable(BuildContext context) {
    return MediaQuery.of(context).platformBrightness == Brightness.dark;
  }
}