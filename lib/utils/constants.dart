class Constants {
  /// Routes name
  static const String loginRoute = 'login';
  static const String homeRoute = 'home';
  static const String groupRoute = 'group';
  static const String profileRoute = 'profile';

  /// Page keys
  static const String loginPageKey = 'LoginPage';
  static const String topPageKey = 'TopPage';
  static const String homePageKey = 'HomePage';
  static const String groupPageKey = 'GroupPage';
  static const String profilePageKey = 'ProfilePage';
  static const String chatSinglePageKey = 'ChatSinglePage';
  static const String notFoundKey = 'NotFoundPage';
}