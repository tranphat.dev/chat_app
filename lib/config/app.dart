import 'package:chat_app/config/messages.dart';

import '../app_state/top_app_state.dart';

abstract class AppConfig {
  static final List<AppMenu> appMenus = <AppMenu>[
    // Home
    AppMenu(
      index: 0,
      title: Messages.homeTitle,
      isEnable: true,
      tab: Tabs.home,
    ),
    // Group
    AppMenu(
      index: 1,
      title: Messages.groupTitle,
      isEnable: true,
      tab: Tabs.group,
    ),
    // Profile
    AppMenu(
      index: 2,
      title: Messages.profileTitle,
      isEnable: true,
      tab: Tabs.profile,
    ),
  ];
}

class AppMenu {
  final int index;
  final String title;
  final bool isEnable;
  final Tabs tab;

  AppMenu({
    required this.title,
    this.isEnable = true,
    required this.index,
    required this.tab,
  });
}
