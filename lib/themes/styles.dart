import 'package:flutter/material.dart';

abstract class Styles {
  static const Color pageBackground = Color(0xFFFFFFFF);

  static const Color formLabelColor = Color(0xFF97ADB6);
  static const Color formErrorColor = Color.fromRGBO(239, 55, 120, 1.0);
  static const Color formHintColor = Color(0xFFBDBDBD);

  static TextStyle formTitleTextStyle(BuildContext context) {
    return Theme.of(context).textTheme.headline4!.merge(const TextStyle(
        color: Styles.formLabelColor, fontWeight: FontWeight.w400));
  }
}