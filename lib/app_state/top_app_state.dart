import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import '../config/app.dart';
import '../router/route_path.dart';

final topAppStateProvider = Provider<TopAppState>((ref) => TopAppState());

enum Tabs {home, group, profile}

class TopAppState extends ChangeNotifier {
  int _currentTabIndex;
  int get currentTabIndex => _currentTabIndex;

  Tabs _currentTab;
  Tabs get currentTab => _currentTab;

  List<BaseRoutePath> _myPageRoute;
  List<BaseRoutePath> get myPageRoute => _myPageRoute;

  TopAppState()
      : _currentTabIndex = 0,
        _currentTab = Tabs.home,
        _myPageRoute = [];

  set currentTabIndex(int index) {
    _currentTabIndex = index;
    List<AppMenu> appMenus = [];
    for (final menu in AppConfig.appMenus) {
      if (menu.isEnable) {
        appMenus.add(menu);
      }
    }
    _currentTab = appMenus.isEmpty ? Tabs.home : appMenus[index].tab;
    handlePopAllMyPage();
    notifyListeners();
  }

  void pushPathToMyPageRoute(BaseRoutePath path) {
    _myPageRoute.add(path);
    notifyListeners();
  }

  /// Handle pop pages of current tab.
  void handlePopPage() {
    if (_myPageRoute.isNotEmpty) {
      handlePopMyPage();
    }
    notifyListeners();
  }

  void handlePopMyPage() {
    _myPageRoute.removeLast();
  }

  void handlePopAllMyPage() {
    _myPageRoute = [];
  }

  /// Pop all pages of each tab after logout.
  void handleDispose() {
    handlePopAllMyPage();
    // Set to first tab.
    currentTabIndex = 0;
  }
}