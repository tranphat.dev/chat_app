import 'package:flutter/material.dart';
import 'package:chat_app/app_state/top_app_state.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

enum Pages { login, register, top }

final mainAppStateProvider = Provider<MainAppState>((ref) => MainAppState(
    ref.read(topAppStateProvider),
));

class MainAppState extends ChangeNotifier {
  final TopAppState topAppState;

  Pages currentPage;

  MainAppState(this.topAppState)
      : currentPage = Pages.login;

  void setCurrentPage(Pages page) {
    currentPage = page;
    notifyListeners();
  }

  void goToTop() => setCurrentPage(Pages.top);

  void logout() {
    topAppState.handleDispose();
    setCurrentPage(Pages.login);
  }
}